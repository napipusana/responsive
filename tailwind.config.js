/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'blue-603EBE': '#603EBE',
        'grey-E7E7E7': '#E7E7E7',
        'grey-C2C2C2': '#C2C2C2',
        'grey-F5F4F9': '#F5F4F9',
        'blue-5E3DB3': '#5E3DB3',
        'blue-090C35': '#090C35'
      },
    }
  },
  plugins: [],
}

